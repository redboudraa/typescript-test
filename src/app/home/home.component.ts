import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
class Country {
    constructor(public name: string) {}
}

let europianCountries = [
    "Austria",
    "Belgium",
    "Bulgaria",
    "Croatia",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Estonia",
    "Finland",
    "France",
    "Germany",
    "Greece",
    "Hungary",
    "Ireland",
    "Italy",
    "Latvia",
    "Lithuania",
    "Luxembourg",
    "Malta",
    "Netherlands",
    "Poland",
    "Portugal",
    "Romania",
    "Slovakia",
    "Slovenia",
    "Spain",
    "Sweden",
    "United Kingdom"
];
@Component({
    moduleId: module.id,
    templateUrl: "./home.component.html",
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
    public countries: Array<String>;

    constructor() {
        this.countries = [];
        let s = "";
        for (let i = 1; i < 10; i++) {
            s = i + ":..............";
            this.countries.push(s);
        }
    }
    ngOnInit(): void {
        // Init your component properties here.
    }
    public onItemTap(args) {
        console.log("Item Tapped at cell index: " + args.index);
    }
}
