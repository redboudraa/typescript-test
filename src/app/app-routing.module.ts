import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { WelcomeComponent } from "./welcome/welcome.component";
import { PasswordComponent } from "./password/password.component";
import { RsetpasswordComponent } from "./rsetpassword/rsetpassword.component";

const routes: Routes = [
    { path: "password", component: PasswordComponent },
    { path: "rsetpassword", component: RsetpasswordComponent },
    { path: "", component: WelcomeComponent },
    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
