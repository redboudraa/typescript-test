import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { WelcomeComponent } from "./welcome/welcome.component";
import { PasswordComponent } from './password/password.component';
import { RsetpasswordComponent } from './rsetpassword/rsetpassword.component';

@NgModule({
    bootstrap: [AppComponent],
    imports: [NativeScriptModule, AppRoutingModule, NativeScriptFormsModule],
    declarations: [AppComponent, WelcomeComponent, PasswordComponent, RsetpasswordComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
