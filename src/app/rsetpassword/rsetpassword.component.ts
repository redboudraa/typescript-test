import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toast";
@Component({
    selector: "ns-rsetpassword",
    templateUrl: "./rsetpassword.component.html",
    styleUrls: ["./rsetpassword.component.css"],
    moduleId: module.id
})
export class RsetpasswordComponent implements OnInit {
    oldpassword = "";
    newpassword = "";
    rpassword = "";
    constructor() {}

    ngOnInit() {}
    check() {
        if (this.oldpassword === "") {
            Toast.makeText(
                "Please enter your old password or click cancel to exit "
            ).show();
        }
        if (this.newpassword === "") {
            Toast.makeText(
                "Please enter your new password or click cancel to exit "
            ).show();
        }
        if (this.newpassword !== this.rpassword) {
            Toast.makeText("password missmatch ").show();
        }
    }
}
