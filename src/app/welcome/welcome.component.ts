import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toast";
import { RouterExtensions } from "nativescript-angular/router";
@Component({
    selector: "ns-welcome",
    templateUrl: "./welcome.component.html",
    styleUrls: ["./welcome.component.css"],
    moduleId: module.id
})
export class WelcomeComponent implements OnInit {
    public password: String = "";
    textFieldData = "";
    constructor(private router: RouterExtensions) {}

    ngOnInit() {}
    passswordbutton() {
        this.router.navigate(["/password"], { clearHistory: false });
    }
    rsetpassswordbutton() {
        this.router.navigate(["/rsetpassword"], { clearHistory: false });
    }
    check() {
        if (this.textFieldData === "") {
            Toast.makeText(
                "Please enter your password or click enter to exit "
            ).show();
        }
    }
}
